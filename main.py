import random
import time

class bcolor:
    WARNING = '\033[93m'
    ENDC = '\033[0m'

if __name__ == '__main__':
    print('''░█▀▀░█░█░░░█▀▀░█░█░█▀█░█░█░▀█▀░░░█▀█░█▀█
░█░█░█░█░░░█░░░█▀█░█░█░█▀▄░░█░░░░█▀▀░█▀█
░▀▀▀░▀▀▀░░░▀▀▀░▀░▀░▀▀▀░▀░▀░▀▀▀░░░▀░░░▀░▀
''')
    
    print('This is a simple console game of stone\nscissors paper, in English-Japanese.')
    print('v.2.0.0')

    print('\n────────────────────────────────────────────────────────────')

    print(bcolor.WARNING + 'Node.' + bcolor.ENDC + ' Game hint\n      ' + bcolor.WARNING + 'グー。Gu。' + bcolor.ENDC + '- stone\n      ' + bcolor.WARNING + 'チョキ。Choki。' + bcolor.ENDC +'- scissors\n      ' + bcolor.WARNING + 'パー。Pa。' + bcolor.ENDC + '- paper\n')

    print(bcolor.WARNING + 'Node.' + bcolor.ENDC  + ' For those who will play with friends\n      First, in order to achieve a common rhythm, we say:\n      ' + bcolor.WARNING + 'さいしよはグー。Sai shiyo wa gū。' + bcolor.ENDC +'\"first the stone\".\n      Further, instead of one, two, three or tsuefa, we say:\n      ' + bcolor.WARNING + 'じゃんけ んぽん。Jan ke n pon。' + bcolor.ENDC +'\n      If the result is the same, then we say:\n      ' + bcolor.WARNING + 'あいこでしよ。Aikodeshi yo。' + bcolor.ENDC)

    print('────────────────────────────────────────────────────────────\n')

    you, bot = 0, 0
    while 1:
        print(bcolor.WARNING + 'Your move:\n1.' + bcolor.ENDC + ' Gu。\n' + bcolor.WARNING + '2.' + bcolor.ENDC + ' Choki。\n' + bcolor.WARNING + '3.' + bcolor.ENDC +' Pa。')
        word = input('Enter word: ')

        if not word:
            print(bcolor.WARNING + '!. Err. You didn\'t enter the word', bcolor.ENDC)
            exit(1)

        if word == 'Gu' or word == 'gu' or word == 'GU' or word == '1':
            you = 0
        elif word == 'Choki' or word == 'choki' or word == 'CHOKI' or word == '2':
            you = 1
        elif word == 'Pa' or word == 'pa' or word == 'PA' or word == '3':
            you = 2
        else:
            print(bcolor.WARNING + '!. Invalid value: ' + word, bcolor.ENDC)
            exit(1)

        bot = random.randint(0, 2)
        
        log = ['ひとつ。', 'ふたつ。', 'みつつ。']
        n = 0
        print(bcolor.WARNING)
        for i in range(len(log)):
            for j in range(len(log[n])):
                print(log[n][j], end='')
                time.sleep(0.2)
            print('')
            n += 1
        print(bcolor.ENDC)

        if you == 2 and bot == 0:
            print('You - ' + bcolor.WARNING + 'パー。' + bcolor.ENDC +' Bot - ' + bcolor.WARNING + 'グー。' + bcolor.ENDC);
            print(bcolor.WARNING + 'あなたが勝つ。Anata ga katsu。' + bcolor.ENDC + 'You win\n');	

        elif you == 0 and bot == 1:    
            print('You - ' + bcolor.WARNING + 'グー。' + bcolor.ENDC +' Bot - ' + bcolor.WARNING + 'チョキ。' + bcolor.ENDC);
            print(bcolor.WARNING + 'あなたが勝つ。Anata ga katsu。' + bcolor.ENDC + 'You win\n');	

        elif you == 1 and bot == 2:
            print('You - ' + bcolor.WARNING + 'チョキ。' + bcolor.ENDC +' Bot - ' + bcolor.WARNING + 'パー。' + bcolor.ENDC);
            print(bcolor.WARNING + 'あなたが勝つ。Anata ga katsu。' + bcolor.ENDC + 'You win\n');	

        elif you == 0 and bot == 2:
            print('You - ' + bcolor.WARNING + 'グー。' + bcolor.ENDC +' Bot - ' + bcolor.WARNING + 'パー。' + bcolor.ENDC);
            print(bcolor.WARNING + 'あなたは負けます。Anata wa makemasu。' + bcolor.ENDC + 'You lose\n');	

        elif you == 1 and bot == 0:
            print('You - ' + bcolor.WARNING + 'チョキ' + bcolor.ENDC +' Bot - ' + bcolor.WARNING + 'グー。' + bcolor.ENDC);
            print(bcolor.WARNING + 'あなたは負けます。Anata wa makemasu。' + bcolor.ENDC + 'You lose\n');	

        elif you == 2 and bot == 1:
            print('You - ' + bcolor.WARNING + 'パー。' + bcolor.ENDC +' Bot - ' + bcolor.WARNING + 'チョキ' + bcolor.ENDC);
            print(bcolor.WARNING + 'あなたは負けます。Anata wa makemasu。' + bcolor.ENDC + 'You lose\n');	

        elif you == bot:
            if you == 1:
                print('You ' + bcolor.WARNING + 'グー。' + bcolor.ENDC + ' Bot ' + bcolor.WARNING + 'グー。' + bcolor.ENDC)
            elif you == 2:
                print('You ' + bcolor.WARNING + 'チョキ' + bcolor.ENDC + ' Bot ' + bcolor.WARNING + 'チョキ' + bcolor.ENDC)
            elif you == 3:
                print('You ' + bcolor.WARNING + 'パー。' + bcolor.ENDC + ' Bot ' + bcolor.WARNING + 'パー。' + bcolor.ENDC)
            print(bcolor.WARNING + 'あいこでしよ。Aiko deshiyo。\n' + bcolor.ENDC)

        print(bcolor.WARNING + '?.' + bcolor.ENDC, end='')
        res = input('Restart (Y/n): ')

        if not res:
            res = 'Y'

        if res == 'Y' or res == 'y':
            ## An empty to go on
            a=1
        elif res == 'N' or res == 'n':
            exit(0)
        else:
            print(bcolor.WARNING + '!. Invalid value: ' + word, bcolor.ENDC)
            exit(1)

        print('────────────────────────────────────────────────────────────\n')

